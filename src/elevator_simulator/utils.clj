(ns elevator-simulator.utils)

(defn ->int
  "Helpter for unsafe integer converting"
  [str]
  (Integer/parseInt str))

(defn ->event
  "Helper for event building"
  ([message]
   (->event message {}))
  ([message data]
   {:msg message :data data}))