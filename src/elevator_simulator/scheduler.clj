(ns elevator-simulator.scheduler
  (:require [clojure.core.async :as a]
            [clojure.tools.logging :as log]
            [com.stuartsierra.component :as component]))

(defn schedule-event
  "Put `event` to `cmd` channel in `timeout` seconds"
  [{:keys [cmd]} timeout event]
  (a/go
    (a/<! (a/timeout (* 1000 timeout)))
    (a/>! cmd event)))

(defrecord Scheduler []
  component/Lifecycle
  (start [this]
    ; use dropping-buffer to avoid blocking due to buffer overloading
    (assoc this :cmd (a/chan (a/dropping-buffer 30))))
  (stop [this]
    (a/close! (:cmd this))
    (assoc this :cmd nil)))

(defn ->scheduler
  "Create scheduler component"
  []
  (->Scheduler))