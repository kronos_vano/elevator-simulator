(ns elevator-simulator.elevator
  (:require [elevator-simulator
             [scheduler :as shed]
             [utils :refer [->event]]]
            [clojure.core.async :as a]
            [clojure.tools.logging :as log]
            [com.stuartsierra.component :as component]))

(def ^:private actions
  {:come-to-floor "Elevator come to the floor #%d"
   :open-doors    "Elevator opened doors at floor #%d"
   :close-doors   "Elevator closed doors at floor #%d"})

(defn press-button
  "Handle elevator's button click"
  [{:keys [shed floors]} target-floor]
  (if (<= 1 target-floor floors)
    (->> (->event :button-pressed {:floor target-floor})
         (a/put! (:cmd shed)))
    (log/error "Invalid floor:" target-floor)))

(defn- close-doors
  "Close elevator's doors"
  [{:keys [doors floor state log-chan]}]
  (a/put! log-chan (->event :loggable-event {:action :close-doors :floor @floor}))
  (reset! doors :closed)
  (swap! state assoc @floor false))

(defn- open-doors
  "Open elevator's doors"
  [{:keys [state upper lower floors time doors floor shed log-chan]}]
  (a/put! log-chan (->event :loggable-event {:action :open-doors :floor @floor}))
  (reset! doors :opened)
  (let [upper' @upper
        lower' @lower]
    (when (= @floor lower')
      (compare-and-set! lower lower' floors))  ; clear lower boundary
    (when (= @floor upper')
      (compare-and-set! upper upper' -1)))     ; clear upper boundary
  (shed/schedule-event shed time (->event :door-closed)))

(defn- move
  "Move elevator to the given floor"
  [{:keys [speed shed floor]} delta]
  (shed/schedule-event shed speed (->event :floor-passed {:floor (+ @floor delta)})))

(defn- stop
  "Stops the elevator"
  [{:keys [upper lower direction floors]}]
  (let [direction' @direction
        upper'     @upper
        lower'     @lower]
    (when (compare-and-set! direction direction' :stand)
      (compare-and-set! lower lower' floors)    ; clear lower boundary
      (compare-and-set! upper upper' -1))))     ; clear upper boundary

(defn- choose-next-action
  [{:keys [state upper lower floor direction] :as <elevator>}]
  (if (nth @state @floor)
    (open-doors <elevator>)
    (let [direction' @direction]
      (if (<= @upper @floor @lower)
        (compare-and-set! direction direction' :stand)
        (case direction'
          :up    (if (> @upper @floor)
                   (move <elevator> 1)
                   (when (compare-and-set! direction direction' :down)
                     (move <elevator> -1)))
          :down  (if (< @lower @floor)
                   (move <elevator> -1)
                   (when (compare-and-set! direction direction' :up)
                     (move <elevator> 1)))
          :stand (if (< @floor @upper)
                   (when (compare-and-set! direction direction' :up)
                     (move <elevator> 1))
                   (when (compare-and-set! direction direction' :down)
                     (move <elevator> -1)))
          (log/error "Invalid direction:" direction'))))))

(defn- press-button-internal
  "Internal handler for elevator button clicks"
  [{:keys [state upper direction lower doors floors] :as <elevator>} target-floor]
  (when-not (nth @state target-floor)
    (swap! state assoc target-floor true)
    (swap! upper #(max % target-floor))
    (swap! lower #(min % target-floor))
    (when (and (= @doors :closed)
               (= @direction :stand))
      (choose-next-action <elevator>))))

(defn- start-event-loop
  [{:keys [state upper lower settings floor shed] :as <elevator>}]
  (a/go-loop []
    (when-let [{:keys [msg data] :as event} (a/<! (:cmd shed))]
      (log/debug "New event:" event)
      (case msg
        :loggable-event (let [{:keys [floor action]} data]
                          (log/info (format (get actions action "Unknown action") (inc floor))))
        :floor-passed   (let [new-floor (:floor data)]
                          (reset! floor new-floor)
                          (a/>! (:cmd shed) (->event :loggable-event {:action :come-to-floor :floor new-floor}))
                          (choose-next-action <elevator>))
        :door-closed    (do
                          (close-doors <elevator>)
                          (choose-next-action <elevator>))
        :button-pressed (press-button-internal <elevator> (-> data :floor dec))
        (log/error "Unknown message:" event))
      (recur))))

(defrecord Elevator [speed floors time sheduler log-chan]
  component/Lifecycle
  (start [this]
    (let [<elevator> (assoc this :state     (atom (into [] (take floors (repeat false))))
                                 :direction (atom :stand)
                                 :doors     (atom :closed)
                                 :upper     (atom -1)
                                 :lower     (atom floors)
                                 :floor     (atom 0)
                                 :shed      sheduler
                                 :log-chan  (or log-chan (:cmd sheduler)))]
      (start-event-loop <elevator>)
      <elevator>))
  (stop [this]
    (assoc this :settings nil :state nil :shed nil)))

(defn ->elevator
  "Create elevator component"
  [{:keys [height speed] :as options}]
  (-> (dissoc options :height)
      (assoc :speed (/ height speed))
      (map->Elevator)))
