(ns elevator-simulator.core
  (:require [clojure.string :as str]
            [elevator-simulator
             [utils :refer [->int]]
             [system :as system]
             [elevator :as elevator]]
            [clojure.tools.cli :as cli]
            [com.stuartsierra.component :as component]
            [clojure.core.async :as a]
            [clojure.tools.logging :as log])
  (:import [org.slf4j.bridge SLF4JBridgeHandler]
           (java.io BufferedReader))
  (:gen-class))

(def ^:private cli-options
  [["-f" "--floors AMOUNT" "Floors amount"
    :default 5
    :parse-fn ->int
    :validate [#(< 4 % 21) "Must be an integer between 5 and 20 inclusively"]]
   ["-s" "--speed SPEED" "Elevator's speed in m/s"
    :default 1
    :parse-fn ->int
    :validate [#(< 0 % 100) "Must be an integer between 5 and 100 inclusively"]]
   ["-e" "--height HEIGHT" "Floor's height in meters"
    :parse-fn ->int
    :validate [#(< 0 % 10000) "Must be an integer between 0 and 10000 inclusively"]
    :default 5]
   ["-t" "--time TIME"  "How long doors become open? In seconds"
    :parse-fn ->int
    :validate [#(< 0 %) "Must be an integer greater than 0"]
    :default 5]
   ["-h" "--help"]])

(def ^:private commands
  (str/join "\n" ["push <number> -- ask elevator to go somewhere"
                  "help          -- print help information"
                  "exit          -- leave the program"]))

(defn usage
  "Usage helper"
  [options-summary]
  (->> ["This is kronos elevator emulation"
        ""
        "Usage: ./run.sh [options]"
        ""
        "Options:"
        options-summary
        ""
        "Commands:"
        ""
        commands]
       (str/join \newline)))

(defn errors->msg
  "Error helper"
  [errors]
  (->> errors
       (str/join \newline)
       (str "The following errors occurred while parsing your command:\n\n")))

(defn exit
  "Meaningful exit"
  ([status]
   (System/exit status))
  ([status msg]
   (println msg)
   (exit status)))

(defn init-logging! []
  "Some java magic for logging"
  (SLF4JBridgeHandler/removeHandlersForRootLogger)
  (SLF4JBridgeHandler/install)
  (Thread/setDefaultUncaughtExceptionHandler
    (reify Thread$UncaughtExceptionHandler
      (uncaughtException [_ thread ex]
        (log/error ex "Uncaught exception on" (.getName thread))))))

(defn run
  "Simulator main loop"
  [options]
  (let [system (atom (system/->system options))
        stop   (fn []
                 (swap! system component/stop-system)
                 (exit 0))]
    (swap! system component/start-system)
    (let [{:keys [elevator]} @system]
      (println "Welcome to kronos elevator simulator! Here is some help:")
      (println commands)
      (doseq [cmd (line-seq (BufferedReader. *in*))]
        (condp re-seq cmd
          #"\Apush\s+\d+\z" (->> (re-find #"\d+" cmd)
                                 ->int
                                 (elevator/press-button elevator))
          #"\Aexit\z"       (stop)
          #"\Ahelp\z"       (println commands)
          #".*"             (log/error "Unknown command:" cmd))))))

(defn -main
  "Simulator entry point"
  [& args]
  (init-logging!)
  (let [{:keys [summary errors options]} (cli/parse-opts args cli-options)]
    (cond
      errors          (exit 1 (errors->msg errors))
      (:help options) (exit 0 (usage summary))
      :success        (run options))))
