(ns elevator-simulator.system
  (:require [elevator-simulator
             [elevator :as elevator]
             [scheduler :as scheduler]]
            [com.stuartsierra.component :as component]))

(defn ->system
  "Create system for given elevator's options"
  [options]
  (component/system-using
    (component/system-map
      :sheduler (scheduler/->scheduler)
      :elevator (elevator/->elevator options))
    {:elevator [:sheduler]}))
