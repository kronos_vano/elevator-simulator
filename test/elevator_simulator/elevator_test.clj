(ns elevator-simulator.elevator-test
  (:require [clojure.test :refer :all]
            [elevator-simulator
             [core-test :as t]
             [elevator :as elevator]
             [scheduler :as scheduler]]
            [clojure.tools.logging :as log]
            [clojure.core.async :as a]))

(use-fixtures :once t/init-system)

(defn reset-state!
  [{:keys [state direction doors upper lower floor floors]}]
  (reset! state     (into [] (take floors (repeat false))))
  (reset! direction :stand)
  (reset! doors     :closed)
  (reset! upper     -1)
  (reset! lower     floors)
  (reset! floor     0))

(deftest press-button-test
  (let [{:keys [elevator]} @t/system
        ch                 (a/chan)
        elevator           (assoc-in elevator [:shed :cmd] ch)]

    (testing "checks floors boundaries"
      (reset-state! elevator)
      (elevator/press-button elevator 0)
      (elevator/press-button elevator (inc (:floors elevator)))
      (elevator/press-button elevator (inc (:floors elevator)))
      (scheduler/schedule-event {:cmd ch} (/ 1 10) "timeout")
      (let [event (a/<!! ch)]
        (is (= event "timeout"))))

    (testing "sends button-pressed event"
      (reset-state! elevator)
      (elevator/press-button elevator 1)
      (let [event (a/<!! ch)]
        (is (= {:msg :button-pressed, :data {:floor 1}} event))))))

(deftest close-doors-test
  (let [{:keys [elevator]} @t/system]
    (testing "sends close-doors event"
      (reset-state! elevator)
      (#'elevator/close-doors elevator)
      (let [event (a/<!! (:log-chan t/system-options))]
        (is (= {:msg :loggable-event, :data {:action :close-doors, :floor 0}} event))))

    (testing "clears doors state"
      (reset-state! elevator)
      (reset! (:doors elevator) :opened)
      (#'elevator/close-doors elevator)
      (is (= :closed @(:doors elevator))))

    (testing "clears floors state"
      (reset-state! elevator)
      (swap! (:state elevator) assoc 0 true)
      (#'elevator/close-doors elevator)
      (is (not (nth @(:state elevator) 0))))))

(deftest open-doors-test
  (let [{:keys [elevator]} @t/system]
    (testing "sends loggable-event event"
      (reset-state! elevator)
      (#'elevator/open-doors elevator)
      (let [event (a/<!! (:log-chan t/system-options))]
        (is (= {:msg :loggable-event, :data {:action :open-doors, :floor 0}} event))))

    (testing "changes doors state"
      (reset-state! elevator)
      (#'elevator/open-doors elevator)
      (is (= :opened @(:doors elevator)))
      (a/<!! (:log-chan t/system-options)))

    (testing "clears lower boundary"
      (reset-state! elevator)
      (let [floor 3]
        (swap! (:state elevator) assoc floor true)
        (reset! (:floor elevator) 3)
        (reset! (:lower elevator) 3)
        (#'elevator/open-doors elevator)
        (is (= (:floors elevator) @(:lower elevator)))
        (a/<!! (:log-chan t/system-options))))

    (testing "clears upper boundary"
      (reset-state! elevator)
      (let [floor 3]
        (swap! (:state elevator) assoc floor true)
        (reset! (:floor elevator) 3)
        (reset! (:upper elevator) 3)
        (#'elevator/open-doors elevator)
        (is (= -1 @(:upper elevator)))
        (a/<!! (:log-chan t/system-options))))

    (testing "schedules door-closed event"
      (reset-state! elevator)
      (let [event (atom nil)]
        (with-redefs [scheduler/schedule-event (fn [_ _ event'] (reset! event event'))]
          (#'elevator/open-doors elevator)
          (is (= {:msg :door-closed, :data {}} @event))))
      (a/<!! (:log-chan t/system-options)))))

(deftest stop-test
  (let [{:keys [elevator]} @t/system]
    (testing "clears lower boundary"
      (reset-state! elevator)
      (reset! (:lower elevator) 3)
      (#'elevator/stop elevator)
      (is (= (:floors elevator) @(:lower elevator))))

    (testing "clears upper boundary"
      (reset-state! elevator)
      (reset! (:upper elevator) 3)
      (#'elevator/stop elevator)
      (is (= -1 @(:upper elevator))))))

(deftest move-test
  (let [{:keys [elevator]} @t/system]
    (testing "schedules floor-passed event"
      (reset-state! elevator)
      (let [event (atom nil)]
        (with-redefs [scheduler/schedule-event (fn [_ _ event'] (reset! event event'))]
          (#'elevator/move elevator 1)
          (is (= {:msg :floor-passed, :data {:floor 1}} @event))
          (#'elevator/move elevator -1)
          (is (= {:msg :floor-passed, :data {:floor -1}} @event)))))))

(deftest choose-next-action-test
  (let [{:keys [elevator]}    @t/system
        {:keys [upper lower floor direction]} elevator]
    (testing "opens doors"
      (reset-state! elevator)
      (let [open (atom false)]
        (with-redefs [elevator/open-doors (fn [_] (reset! open true))]
          (swap! (:state elevator) assoc 0 true)
          (#'elevator/choose-next-action elevator)
          (is @open))))

    (testing "does not open doors without need"
      (reset-state! elevator)
      (let [open (atom false)]
        (with-redefs [elevator/open-doors (fn [_] (reset! open true))]
          (#'elevator/choose-next-action elevator)
          (is (not @open)))))

    (testing "stops elevator without jobs"
      (reset-state! elevator)
      (reset! (:direction elevator) :up)
      (#'elevator/choose-next-action elevator)
      (is (= :stand @(:direction elevator))))

    (testing "when elevator direction is up"
      (testing "moves elevator up"
        (reset-state! elevator)
        (reset! direction :up)
        (reset! upper 1)
        (let [moved-up (atom false)]
          (with-redefs [elevator/move (fn [_ delta] (reset! moved-up (= delta 1)))]
            (#'elevator/choose-next-action elevator)
            (is @moved-up)
            (is (= :up @direction)))))

      (testing "moves elevator down"
        (reset-state! elevator)
        (reset! direction :up)
        (reset! upper 1)
        (reset! floor 1)
        (reset! lower 0)
        (let [moved-down (atom false)]
          (with-redefs [elevator/move (fn [_ delta] (reset! moved-down (= delta -1)))]
            (#'elevator/choose-next-action elevator)
            (is @moved-down)
            (is (= :down @direction))))))

    (testing "when elevator direction is down"
      (testing "moves elevator up"
        (reset-state! elevator)
        (reset! direction :down)
        (reset! upper 1)
        (let [moved-up (atom false)]
          (with-redefs [elevator/move (fn [_ delta] (reset! moved-up (= delta 1)))]
            (#'elevator/choose-next-action elevator)
            (is @moved-up)
            (is (= :up @direction)))))

      (testing "moves elevator down"
        (reset-state! elevator)
        (reset! direction :down)
        (reset! upper 1)
        (reset! floor 1)
        (reset! lower 0)
        (let [moved-down (atom false)]
          (with-redefs [elevator/move (fn [_ delta] (reset! moved-down (= delta -1)))]
            (#'elevator/choose-next-action elevator)
            (is @moved-down)
            (is (= :down @direction))))))

    (testing "when elevator direction is stand"
      (testing "moves elevator up"
        (reset-state! elevator)
        (reset! upper 3)
        (reset! floor 2)
        (reset! lower 0)
        (let [moved-up (atom false)]
          (with-redefs [elevator/move (fn [_ delta] (reset! moved-up (= delta 1)))]
            (#'elevator/choose-next-action elevator)
            (is @moved-up)
            (is (= :up @direction)))))

      (testing "moves elevator down"
        (testing "moves elevator up"
          (reset-state! elevator)
          (reset! upper 3)
          (reset! floor 3)
          (reset! lower 0)
          (let [moved-down (atom false)]
            (with-redefs [elevator/move (fn [_ delta] (reset! moved-down (= delta -1)))]
              (#'elevator/choose-next-action elevator)
              (is @moved-down)
              (is (= :down @direction)))))))))

(deftest press-button-internal-test
  (let [{:keys [elevator]} @t/system]
    (testing "assigns new job"
      (reset-state! elevator)
      (reset! (:direction elevator) :up)
      (let [state (:state elevator)]
        (is (not (nth @state 0)))
        (#'elevator/press-button-internal elevator 0)
        (is (nth @state 0))))

    (testing "changes upper boundary"
      (reset-state! elevator)
      (reset! (:direction elevator) :up)
      (reset! (:doors elevator) :opened)
      (let [upper (:upper elevator)]
        (is (= -1 @upper))
        (#'elevator/press-button-internal elevator 0)
        (is (= 0 @upper))
        (#'elevator/press-button-internal elevator 3)
        (is (= 3 @upper))))

    (testing "changes lower boundary"
      (reset-state! elevator)
      (reset! (:direction elevator) :up)
      (reset! (:doors elevator) :opened)
      (let [{:keys [floors lower]} elevator]
        (is (= floors @lower))
        (#'elevator/press-button-internal elevator 3)
        (is (= 3 @lower))
        (#'elevator/press-button-internal elevator 4)
        (is (= 3 @lower))))

    (testing "fires movement"
      (reset-state! elevator)
      (let [action (atom false)]
        (with-redefs [elevator/choose-next-action (fn [_] (reset! action true))]
          (#'elevator/press-button-internal elevator 3)
          (is @action))))

    (testing "does not fires movement when doors are opened"
      (reset-state! elevator)
      (reset! (:direction elevator) :up)
      (reset! (:doors elevator) :opened)
      (let [action (atom false)]
        (with-redefs [elevator/choose-next-action (fn [_] (reset! action true))]
          (#'elevator/press-button-internal elevator 3)
          (is (not @action)))))

    (testing "does not fires movement when elevator is not in stand mode"
      (reset-state! elevator)
      (reset! (:direction elevator) :up)
      (let [action (atom false)]
        (with-redefs [elevator/choose-next-action (fn [_] (reset! action true))]
          (#'elevator/press-button-internal elevator 3)
          (is (not @action)))))))
