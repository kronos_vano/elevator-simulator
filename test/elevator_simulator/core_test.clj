(ns elevator-simulator.core-test
  (:require [clojure.test :refer :all]
            [elevator-simulator
             [core :refer :all]
             [system :as system]]
            [com.stuartsierra.component :as component]
            [clojure.core.async :as a]))

(def system (atom nil))
(def system-options
  {:speed 1 :floors 5 :time 1 :height 1 :log-chan (a/chan 30)})

(defn init-system
  [test-fn]
  (let [system' (system/->system system-options)]
    (reset! system (component/start system'))
    (test-fn)
    (swap! system component/stop)))

(deftest a-test
  (testing "FIXME, I fail."
    (is (= 1 1))))
