# Elevator simulator

A simple elevator simulator written in Clojure

## Usage

### Running
```bash
./run --floors 5 --speed 1 --height 5 --time 10
```

### Possible commands
```
push 5 # Simulate button press inside elevator or in front of the elevator
exit   # Exit simulator
```

## Compile

```bash
lein uberjar
```

## Run tests

```bash
lein test
```

## License

Copyright © 2017 Samsonov Ivan

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
